package com.example.rax.apprestriction.manage.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.example.rax.apprestriction.manage.constants.Priority
import com.example.rax.apprestriction.manage.util.CalendarUtil

@Entity(tableName = "app")
data class App(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int = 0,
        @ColumnInfo(name = "appName") var appName: String = "",
        @ColumnInfo(name = "packageName") var packageName: String = "",
        @ColumnInfo(name = "endTime") var endTime: Long = CalendarUtil.getEndOfDay(), // enter timestamp of end day time
        @ColumnInfo(name = "totalTime") var totalTime: Long = 0, // total time of usage
        @ColumnInfo(name = "timeAllowed") var timeAllowed: Long = 0, // total time allowed for the app to be used
        @ColumnInfo(name = "decreaser") var decreaser: Int = 1, // decrease the @timeAllowed accroding to this
        @ColumnInfo(name = "location") var location: String = "{\n" +
                "\"location\": [\n" +
                "{\n" +
                "\"lat\": 0.0,\n" +
                "\"long\": 0.0\n" +
                "\"name\": \"\"\n" +
                "\"radius\": 0.0\n" +
                "}\n" +
                "]\n" +
                "}",
        @ColumnInfo(name = "priority") var priority: Int = Priority.TIME,
        @ColumnInfo(name = "rule") var rule: String = "")