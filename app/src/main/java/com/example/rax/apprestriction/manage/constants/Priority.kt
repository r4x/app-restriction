package com.example.rax.apprestriction.manage.constants

object Priority {
    const val BLOCK = 3
    const val LOCATION = 2
    const val TIME = 1
}