package com.example.rax.apprestriction.manage.restriction.worker

import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.rax.apprestriction.manage.constants.Location
import com.example.rax.apprestriction.manage.database.DatabaseHelper
import com.example.rax.apprestriction.manage.database.entity.App
import com.example.rax.apprestriction.manage.model.LatLong
import com.example.rax.apprestriction.manage.restriction.util.addUsage
import com.google.gson.Gson
import java.util.*

class RestrictionWorkerImpl(val context: Context) : RestrictionWorker {

    private lateinit var app: App
    private var currentTime: Long = 0
    private var timer = Timer()
    private val databaseHelper by lazy { DatabaseHelper.getInstance() }


    override fun addAppUsage() {
        if (app.totalTime >= app.timeAllowed) {
            goToHome()
        } else {
            timer = Timer()
            var isWarned = false
            timer.addUsage {
                with(app) {
                    totalTime += 1000
                    Log.e("diskyoon", "time :" + app.totalTime)
                    if (totalTime >= timeAllowed) {
                        if (!isWarned) {
                            isWarned = true
                            // kill the app
                            goToHome()
                        }
                    }
                }
            }
        }

    }

    private fun goToHome() {
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        com.example.rax.apprestriction.App.get().startActivity(startMain)
    }

    override fun stopAppUsage() {
        if (app.packageName.isNotBlank()) {
            timer.cancel()
            databaseHelper.updateApp(app)
        }
    }

    override fun checkLocation(app: App) {
        val locations = Gson().fromJson(this.app.location, LatLong::class.java)
        var isAtLocation = false
        for (location in locations.location) {
            if (location.lat >= Location.latitude + location.radius && location.lat <= Location.latitude + location.radius
                    && location.long >= Location.longitude + location.radius && location.long <= Location.longitude + location.radius) {
                isAtLocation = true
                goToHome()
            }
        }
        if (!isAtLocation) {
            if (app.packageName.isNotBlank()) {
                this.app = app
                addAppUsage()
            }
        }
    }

    override fun blockApp() {
        goToHome()
    }

    override fun initWorker(app: App) {
        this.app = app
        currentTime = System.currentTimeMillis()
    }


}
