package com.example.rax.apprestriction.manage.constants

object Rule {
    const val location = "location"
    const val time = "time"
    /*const val day = "day"*/
    const val block = "block"
}