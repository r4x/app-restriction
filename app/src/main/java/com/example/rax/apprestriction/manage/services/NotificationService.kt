package com.example.rax.apprestriction.manage.services

import android.annotation.SuppressLint
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification


@SuppressLint("NewApi")
class NotificationService : NotificationListenerService() {


    override fun onNotificationPosted(sbn: StatusBarNotification) {
        cancelNotification(sbn.key)
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification) {

    }
}