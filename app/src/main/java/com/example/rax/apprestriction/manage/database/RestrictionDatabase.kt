package com.example.rax.apprestriction.manage.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.example.rax.apprestriction.manage.database.dao.AppDao
import com.example.rax.apprestriction.manage.database.dao.EmergencyDao
import com.example.rax.apprestriction.manage.database.entity.App

@Database(entities = [(App::class)], version = 1)
abstract class RestrictionDatabase : RoomDatabase() {

    abstract fun appDao(): AppDao

    abstract fun emergencyDao(): EmergencyDao

    companion object {
        private var INSTANCE: RestrictionDatabase? = null

        fun getInstance(context: Context): RestrictionDatabase? {
            if (INSTANCE == null) {
                synchronized(RestrictionDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                            RestrictionDatabase::class.java, "restriction.db")
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}