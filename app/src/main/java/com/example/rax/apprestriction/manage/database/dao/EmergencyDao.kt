package com.example.rax.apprestriction.manage.database.dao

import android.arch.persistence.room.*
import com.example.rax.apprestriction.manage.database.entity.App
import com.example.rax.apprestriction.manage.database.entity.Emergency

@Dao
interface EmergencyDao : BaseDao<Emergency> {

    @Query("select * from emergency limit 0,1")
    fun getEmergency(): Emergency?

    @Query("delete from emergency")
    fun nukeTable()

}
