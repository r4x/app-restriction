package com.example.rax.apprestriction.manage.foregroundChecker.detectors;


import android.content.Context;

public interface Detector {
    String getForegroundApp(Context context);
}
