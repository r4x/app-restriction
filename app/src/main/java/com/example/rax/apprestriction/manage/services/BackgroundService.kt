package com.example.rax.apprestriction.manage.services

import android.annotation.SuppressLint
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.location.Location
import android.os.*
import android.support.v4.app.NotificationCompat
import com.example.rax.apprestriction.manage.constants.Location as LocationConst
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.example.rax.apprestriction.LockActivity
import com.example.rax.apprestriction.R
import com.example.rax.apprestriction.manage.constants.Work
import com.example.rax.apprestriction.manage.foregroundChecker.Utils
import com.example.rax.apprestriction.manage.foregroundChecker.detectors.LollipopDetector
import com.example.rax.apprestriction.manage.foregroundChecker.detectors.PreLollipopDetector
import com.example.rax.apprestriction.manage.restriction.proxy.Restriction
import com.example.rax.apprestriction.manage.restriction.proxy.RestrictionImpl
import com.example.rax.apprestriction.manage.util.CalendarUtil
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationServices.getFusedLocationProviderClient


class BackgroundService : Service(), LocationListener,
        GoogleApiClient.ConnectionCallbacks {

    // location
    private val INTERVAL = (1000 * 2).toLong()
    private val FASTEST_INTERVAL = (1000 * 1).toLong()
    private var mLocationRequest: LocationRequest? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var speed: Double = 0.0
    var mLocationCallback: LocationCallback? = null
    var isdriving: Boolean = false

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest?.interval = INTERVAL
        mLocationRequest?.fastestInterval = FASTEST_INTERVAL
        mLocationRequest?.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    override fun onLocationChanged(location: Location?) {
        LocationConst.latitude = location?.latitude ?: 0.0
        LocationConst.longitude = location?.longitude ?: 0.0
        speed = (location?.speed!! * 18 / 5).toDouble()
        if (speed > 10) {
            val dialog = showDrivingDialog()
            if (!dialog.isShowing) {
                dialog.show()
            }
        } else if (speed == 0.0) {
            if (isdriving) {
                val dialog = showDrivingDialog()
                dialog.show()
            }
        }
    }


    override fun onConnected(p0: Bundle?) {
        try {
            getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest,
                    getLocationCallback(),
                    Looper.myLooper())
        } catch (e: SecurityException) {

        }

    }

    override fun onConnectionSuspended(p0: Int) {

    }

    private fun showDrivingDialog(): AlertDialog {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Are you driving?")
        alertDialogBuilder.setMessage("If driving please tell us we will block all apps for you to avoid distraction.")
        alertDialogBuilder.setPositiveButton("Yes",
                { _, _ -> restrictor.execute(Work.startDriving) })
        alertDialogBuilder.setNegativeButton("No",
                { _, _ ->
                    restrictor.execute(Work.endDriving)
                })
        return alertDialogBuilder.create().also {
            if (Utils.postOreo()) {
                it.window.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY)
            } else {
                it.window.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
            }
        }
    }


    private fun getLocationCallback(): LocationCallback {
        if (mLocationCallback == null) {

            mLocationCallback = object : LocationCallback() {
                override fun onLocationResult(p0: LocationResult?) {
                    super.onLocationResult(p0)
                }
            }
        }
        return mLocationCallback!!
    }

    // restriction
    private var lastPackage = ""
    private var currentPackage = ""
    lateinit var runnable: Runnable

    private val TAG_FOREGROUND_SERVICE: String = javaClass.simpleName
    val NOTIFICATION_CHANNEL_ID = "10001"
    lateinit var restrictor: Restriction

    private val detector = if (Utils.postLollipop())
        LollipopDetector()
    else
        PreLollipopDetector()


    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        restrictor = RestrictionImpl(this)
        Log.d("BackgroundService", "BackgroundService: onStartCommand called - ")
        if (intent != null) {
            when (intent.action) {
                ACTION_START -> startForegroundService()
                ACTION_STOP -> stopForegroundService()
            }
        }
        init()
        return START_STICKY
    }

    @SuppressLint("ShowToast")
    private fun initPackageChecker() {
        runnable = Runnable {
            handler.postDelayed(runnable, 600)
            currentPackage = detector.getForegroundApp(this) ?: ""
            if (lastPackage != currentPackage) {
                restrictor.execute(Work.end, lastPackage)
                restrictor.execute(Work.checkAppRestricted, currentPackage)
                { isRestricted, isDriving, emergency ->
                    if (isDriving) {
                        isdriving = true
                        // Stop all apps

                    } else {
                        isdriving = false
                        if (emergency.isEmergency) {
                            if (emergency.emergencyEndTime < System.currentTimeMillis()) {

                                Toast.makeText(this, "Emergency", Toast.LENGTH_LONG).show()
                                // TODO: 8/6/18 check current Time and emergency endTime. if end TIme cross, update emergency to false.
                            }
                        } else {
                            if (isRestricted) {
                                restrictor.execute(Work.start, currentPackage)
                            }
                        }
                    }
                }
            }
            lastPackage = currentPackage
        }
        handler.postDelayed(runnable, 250)
    }


    @SuppressLint("NewApi")
    override fun onTaskRemoved(rootIntent: Intent) {

        init(applicationContext)

        super.onTaskRemoved(rootIntent)
    }

    private fun init() {
        registerReceiver(ScreenOnOffReceiver(), screenOnOffFilter())
    }

    private fun screenOnOffFilter(): IntentFilter {
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        return filter
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("BackgroundService", "onDestroy called")
    }


    private inner class ScreenOnOffReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == Intent.ACTION_SCREEN_ON) {
                Log.d("BackgroundService", "ACTION_SCREEN_ON")
                start()
            } else if (intent.action == Intent.ACTION_SCREEN_OFF) {
                Log.d("BackgroundService", "ACTION_SCREEN_OFF")
                lastPackage = ""
                stop()
                //stop(this@BackgroundService)
            }
        }
    }

    /* Used to build and init foreground service. */
    private fun startForegroundService() {
        Log.d(TAG_FOREGROUND_SERVICE, "Start foreground service.")
        initPackageChecker()
        val intent = Intent()
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val mBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this)
        val mNotificationManager: NotificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        mBuilder.setSmallIcon(R.mipmap.ic_launcher)
        mBuilder.setContentTitle(getString(R.string.app_name))
                .setContentText("Taking Care of Your Pledge.")
                .setAutoCancel(false)
                //.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(pendingIntent)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID)
            mNotificationManager.createNotificationChannel(notificationChannel)
        }


        // Start foreground service.
        startForeground(1, mBuilder.build())
    }


    private fun startLockActivity() {
        val intent = Intent(this, LockActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
    }

    private fun stopForegroundService() {
        Log.d(TAG_FOREGROUND_SERVICE, "Stop foreground service.")
        handler.removeCallbacksAndMessages(null)
        // Stop foreground service and remove the notification.
        stopForeground(true)

        // Stop the foreground service.
        stopSelf()
    }

    fun start() {
        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build()
        mGoogleApiClient?.connect()
        initPackageChecker()
    }

    fun stop() {
        handler.removeCallbacksAndMessages(null)
        restrictor.execute(Work.end, currentPackage)
        stopLocationUpdates()
        if (mGoogleApiClient?.isConnected == true) {
            mGoogleApiClient?.disconnect()
        }
    }

    private fun stopLocationUpdates() {
        getFusedLocationProviderClient(this).removeLocationUpdates(getLocationCallback())
    }

    companion object {
        private val handler: Handler = Handler()
        const val ACTION_START = "com.applock.intent.action.init"
        const val ACTION_STOP = "com.applock.intent.action.stop"
        private const val REQUEST_CODE = 1001
        private var appLockServiceIntent: PendingIntent? = null
        private var appRestrictIntent: PendingIntent? = null
        private var isAlarmStarted = false


        fun init(context: Context) {
            val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            am.setExact(AlarmManager.ELAPSED_REALTIME, 250, getRunIntent(context))
            am.setRepeating(AlarmManager.ELAPSED_REALTIME, CalendarUtil.getEndOfDay(), AlarmManager.INTERVAL_DAY, getRestrictionUpdateIntent(context))
            isAlarmStarted = true
        }

        private fun getRestrictionUpdateIntent(context: Context): PendingIntent {
            if (appRestrictIntent == null) {
                val intent = Intent(context, RestrictionUpdateBroadcast::class.java)
                appRestrictIntent = PendingIntent.getBroadcast(
                        context, 280192, intent, PendingIntent.FLAG_CANCEL_CURRENT)
            }
            return appRestrictIntent!!
        }

        private fun getRunIntent(context: Context): PendingIntent {
            if (appLockServiceIntent == null) {
                val intent = Intent(context, BackgroundService::class.java)
                intent.action = ACTION_START
                appLockServiceIntent = PendingIntent.getService(context, REQUEST_CODE, intent, 0)
            }
            return appLockServiceIntent!!
        }
    }
}