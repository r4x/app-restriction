package com.example.rax.apprestriction.manage.constants

object Work {
    const val start = "init"
    const val end = "end"
    const val checkAppRestricted = "checkAppRestricted"
    const val startDriving = "startDriving"
    const val endDriving = "endDriving"
}