package com.example.rax.apprestriction.manage.restriction.worker

import com.example.rax.apprestriction.manage.database.entity.App

interface RestrictionWorker {
    fun initWorker(app: App)
    fun addAppUsage()
    fun stopAppUsage()
    fun checkLocation(app: App)
    fun blockApp()
}