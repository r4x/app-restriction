package com.example.rax.apprestriction.manage.restriction.proxy

import android.content.Context
import com.example.rax.apprestriction.manage.constants.Work
import com.example.rax.apprestriction.manage.database.entity.Emergency
import com.example.rax.apprestriction.manage.restriction.interactor.RestrictionInteractor

class RestrictionImpl(context: Context) : Restriction {
    private val restriction by lazy { RestrictionInteractor(context) }

    override fun execute(work: String, packageName: String, param: (Boolean, Boolean, Emergency) -> Unit) {
        restriction.initInteractor(packageName)
        when (work) {
            Work.start -> {
                restriction.start()
            }
            Work.end -> {
                restriction.finish()
            }
            Work.checkAppRestricted -> {
                restriction.checkIsAppRestricted(param)
            }
            Work.startDriving -> {
                restriction.startDriving()
            }
            Work.endDriving -> {
                restriction.stopDriving()
            }
        }
    }

}