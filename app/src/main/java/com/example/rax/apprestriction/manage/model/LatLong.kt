package com.example.rax.apprestriction.manage.model


data class LatLong(
        val location: List<Location>
) {

    data class Location(
            val lat: Double,
            val long: Double,
            val name: String,
            val radius: Double
    )
}