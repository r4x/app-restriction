package com.example.rax.apprestriction.manage.restriction.proxy

import com.example.rax.apprestriction.manage.database.entity.Emergency


interface Restriction {
    fun execute(work: String, packageName: String = "", param: (Boolean, Boolean, Emergency) -> Unit = { isRestricted, isDriving, emergency -> })
}