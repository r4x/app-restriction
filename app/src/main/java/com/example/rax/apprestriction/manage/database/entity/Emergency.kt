package com.example.rax.apprestriction.manage.database.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "emergency")
data class Emergency(
        @ColumnInfo(name = "emergencyCount") var emergencyCount: Int = 3, // in emergency user is allowed to use the app for @see emergencyTime
        @ColumnInfo(name = "emergencyTotalAllowedTime") var emergencyTotalAllowedTime: Int = 10, // total emergency time
        @ColumnInfo(name = "emergencyStartTime") var emergencyStartTime: Long = 0, // emergency start time
        @ColumnInfo(name = "emergencyEndTime") var emergencyEndTime: Long = 0, // decrease emergencyCount by 1  if end time is over and set @see isEmergenct to false
        @ColumnInfo(name = "isEmergency") var isEmergency: Boolean = false // to check whether the user has emeregency or not
)
