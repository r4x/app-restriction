package com.example.rax.apprestriction.manage.foregroundChecker.detectors

import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager

class PreLollipopDetector : Detector {
    override fun getForegroundApp(context: Context): String? {
        val am = context.getSystemService(Service.ACTIVITY_SERVICE) as ActivityManager
        val foregroundTaskInfo = am.getRunningTasks(1)[0]
        val foregroundTaskPackageName = foregroundTaskInfo.topActivity.packageName
        val pm = context.packageManager
        var foregroundAppPackageInfo: PackageInfo? = null
        try {
            foregroundAppPackageInfo = pm.getPackageInfo(foregroundTaskPackageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
            return null
        }

        var foregroundApp: String? = null
        if (foregroundAppPackageInfo != null)
            foregroundApp = foregroundAppPackageInfo.applicationInfo.packageName

        return foregroundApp
    }
}
