package com.example.rax.apprestriction.manage.database.dao

import android.arch.persistence.room.*
import com.example.rax.apprestriction.manage.database.entity.App

@Dao
interface AppDao : BaseDao<App> {

    @Query("select * from app")
    fun getAllApps(): List<App>?

    @Query("select * from app where rule = :rule")
    fun getAllApps(rule: String): List<App>?

    @Query("select * from app where packageName = :packageName")
    fun getApp(packageName: String): App?

    @Query("select * from app where packageName = :packageName")
    fun getApps(packageName: String): List<App>?

    @Query("select * from app where packageName = :packageName and rule = :rule")
    fun getApp(packageName: String, rule: String): App?

    @Query("delete from app")
    fun nukeTable()

}
