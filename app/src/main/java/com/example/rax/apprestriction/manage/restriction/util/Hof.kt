package com.example.rax.apprestriction.manage.restriction.util

import android.content.Context
import java.util.*
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future

inline fun Timer.addUsage(crossinline body: () -> Unit) {
    scheduleAtFixedRate(object : TimerTask() {

        override fun run() {
            body.invoke()
        }

    }, 0, 1000)
}

val Context.executer get() = Executors.newFixedThreadPool(2)

inline fun <T> Context.workerSync(crossinline body: () -> T): T {
    val future: Future<T> = executer.submit(Callable<T> {
        body()
    })

    return future.get()
}