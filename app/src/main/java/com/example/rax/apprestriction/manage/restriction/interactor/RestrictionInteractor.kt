package com.example.rax.apprestriction.manage.restriction.interactor

import android.content.Context
import com.example.rax.apprestriction.manage.constants.Priority
import com.example.rax.apprestriction.manage.constants.Rule
import com.example.rax.apprestriction.manage.database.DatabaseHelper
import com.example.rax.apprestriction.manage.database.entity.App
import com.example.rax.apprestriction.manage.database.entity.Emergency
import com.example.rax.apprestriction.manage.restriction.util.workerSync
import com.example.rax.apprestriction.manage.restriction.worker.RestrictionWorker
import com.example.rax.apprestriction.manage.restriction.worker.RestrictionWorkerImpl

class RestrictionInteractor(val context: Context) {
    private var rule = ""
    private val worker: RestrictionWorker by lazy { RestrictionWorkerImpl(context) }
    private var app: App? = null
    private var apps: List<App> = listOf(App())
    private var emergency: Emergency? = null
    private var isDriving = false
    private val databaseHelper by lazy { DatabaseHelper.getInstance() }

    /**
     * This method perform init action according to rule
     */
    fun start() {
        when (rule) {
            Rule.time -> {
                worker.addAppUsage()
            }
            Rule.location -> {
                val app = apps.find { it.priority == Priority.TIME } ?: App()
                worker.checkLocation(app)
            }
            Rule.block -> {
                worker.blockApp()
            }
        }
    }

    /**
     * This method perform end action according to rule
     */
    fun finish() {
        when (rule) {
            Rule.time -> {
                worker.stopAppUsage()
            }
            Rule.location -> {
                worker.stopAppUsage()
            }
            Rule.block -> {
//do nothing
            }
        }
    }


    /**
     * @param packageName required to get app from database
     * method initialise app and it rule and current timestamp
     */
    fun initInteractor(packageName: String) {
        initializeAppAndEmergency(packageName)
    }

    private fun initializeAppAndEmergency(packageName: String) {
        context.workerSync {
            if (app?.packageName != packageName) {
                apps = databaseHelper.getApps(packageName)
                        .run {
                            if (this != null && isNotEmpty())
                                sortedByDescending { it.priority }
                            else listOf(App())
                        }
                app = apps.first()
            }
        }
        rule = app?.rule ?: ""
        worker.initWorker(app!!)
        context.workerSync {
            emergency = databaseHelper.getEmergency()
        }
    }

    /**
     * @param body contain Boolean and String
     * Boolean - return true or false according to app is restricted or not.
     * String  - return emergency or driving status.
     */
    fun checkIsAppRestricted(body: (Boolean, Boolean, Emergency) -> Unit) {
        if (app?.packageName?.isNotBlank()!!) {
            body(true, isDriving, emergency ?: Emergency())
        } else {
            body(false, isDriving, emergency ?: Emergency())
        }
    }

    /**
     * Will stop all the apps*/
    fun startDriving() {
        isDriving = true
    }

    /**
     * Will start all the apps except restricted apps*/
    fun stopDriving() {
        isDriving = false
    }

}
