package com.example.rax.apprestriction.manage.database

import com.example.rax.apprestriction.manage.database.entity.App
import com.example.rax.apprestriction.manage.database.entity.Emergency
import com.example.rax.apprestriction.App as Application

class DatabaseHelper private constructor() {

    companion object {
        private val instan by lazy { DatabaseHelper() }
        fun getInstance() = instan
    }

    private val getAppDao by lazy { RestrictionDatabase.getInstance(Application.get())!!.appDao() }
    private val getEmergencyDao by lazy { RestrictionDatabase.getInstance(Application.get())!!.emergencyDao() }

    fun getAllApps(): List<App>? {
        return getAppDao.getAllApps()
    }

    fun getAllApps(rule: String): List<App>? {
        return getAppDao.getAllApps(rule)
    }

    fun getApp(packageName: String): App? {
        return getAppDao.getApp(packageName)
    }

    fun getApp(packageName: String, rule: String): App? {
        return getAppDao.getApp(packageName, rule)
    }

    fun getApps(packageName: String): List<App>? {
        return getAppDao.getApps(packageName)
    }

    fun insertApp(app: App) {
        getAppDao.insert(app)
    }

    fun updateApp(app: App) {
        getAppDao.update(app)
    }

    fun deleteApp(app: App) {
        getAppDao.delete(app)
    }

    fun getEmergency(): Emergency? {
        return getEmergencyDao.getEmergency()
    }

    fun insertEmergency(emergency: Emergency) {
        getEmergencyDao.insert(emergency)
    }

    fun updateEmergency(emergency: Emergency) {
        getEmergencyDao.update(emergency)
    }
}