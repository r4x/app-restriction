package com.example.rax.apprestriction.manage.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.rax.apprestriction.manage.constants.Rule
import com.example.rax.apprestriction.manage.database.DatabaseHelper

class RestrictionUpdateBroadcast : BroadcastReceiver() {
    private val databaseHelper: DatabaseHelper = DatabaseHelper.getInstance()
    override fun onReceive(context: Context?, intent: Intent?) {
        val apps = databaseHelper.getAllApps(Rule.time)
        apps?.forEach {
            it.totalTime = 0
            if (it.timeAllowed > 900000) {
                it.timeAllowed -= it.decreaser
            }
            databaseHelper.updateApp(it)
        }

    }
}